package ru.tsc.fuksina.tm.command.task;

import ru.tsc.fuksina.tm.model.Task;
import ru.tsc.fuksina.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-project-id";

    public static final String DESCRIPTION = "Show task by project id";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);
        renderTasks(tasks);
    }

}
