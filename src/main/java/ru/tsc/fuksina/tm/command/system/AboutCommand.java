package ru.tsc.fuksina.tm.command.system;

public final class AboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Display developer info";

    public static final String ARGUMENT = "-a";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Author: Yana Fuksina");
        System.out.println("Email: yfuksina@t1-consulting.ru");
    }

}
