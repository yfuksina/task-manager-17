package ru.tsc.fuksina.tm.command.task;

import ru.tsc.fuksina.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractTaskCommand {

    public static final String NAME = "task-create";

    public static final String DESCRIPTION = "Create new task";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        System.out.println("ENTER START DATE:");
        final Date dateStart = TerminalUtil.nextDate();
        System.out.println("ENTER END DATE:");
        final Date dateEnd = TerminalUtil.nextDate();
        getTaskService().create(name, description, dateStart, dateEnd);
    }

}
