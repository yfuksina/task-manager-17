package ru.tsc.fuksina.tm.command.project;

import ru.tsc.fuksina.tm.api.service.IProjectService;
import ru.tsc.fuksina.tm.command.AbstractCommand;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("START DATE: " + DateUtil.toString(project.getDateStart()));
        System.out.println("END DATE: " + DateUtil.toString(project.getDateEnd()));
    }

}
