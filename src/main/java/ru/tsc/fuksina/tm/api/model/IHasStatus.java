package ru.tsc.fuksina.tm.api.model;

import ru.tsc.fuksina.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
